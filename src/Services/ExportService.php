<?php

namespace Whiz\Laravel\API\Services;

use Exception;

class ExportService extends BaseService
{
  /**
   * ExportService constructor.
   * @throws Exception
   */
  public function __construct()
  {
    parent::__construct();
    $this->client->addHeader('Authorization', 'Bearer ' . $this->accessToken);
  }

  /**
   * @param array $filters
   * @return mixed
   */
  public function getFiles($filters = [])
  {
    return $this->client->get($this->url . '/v1/excel/files', $filters);
  }

  /**
   * @param $data
   * @return mixed
   * @throws Exception
   */
  public function generateExcelFileStart($data)
  {
    return $this->client->post($this->url . '/v1/excel/headers', $data);
  }

  /**
   * @param $uuid
   * @param $data
   * @return mixed
   * @throws Exception
   */
  public function generateExcelFileAddData($uuid, $data)
  {
    return $this->client->patch($this->url . '/v1/excel/' . $uuid . '/data', $data);
  }

  /**
   * @param $uuid
   * @param $data
   * @return mixed
   * @throws Exception
   */
  public function generateExcelFileStop($uuid, $data)
  {
    return $this->client->patch($this->url . '/v1/excel/' . $uuid . '/close', $data);
  }
}
