<?php

namespace Whiz\Laravel\API\Services;

use Exception;
use Throwable;

class LocalizationService extends BaseService
{
    private $localizationProvider;

    /**
     * LocalizationService constructor.
     * @throws Exception
     * @throws Throwable
     */
    public function __construct()
    {
        parent::__construct();
        $this->localizationProvider = config('whiz.providers.localization');
        $this->client->addHeader('Authorization', 'Bearer '.$this->accessToken);
    }

    public function zoneByProvider($lng, $lat)
    {
        $data = [
            'longitude' => $lng,
            'latitude' => $lat,
        ];
        return $this->client->get($this->url . '/v1/localization/zone/' . $this->localizationProvider, $data);
    }

    public function dangerZone($lng, $lat)
    {
        $data = [
            'longitude' => $lng,
            'latitude'  => $lat,
            'type'      => 'danger'
        ];
        $this->client->addHeader('Authorization', 'Bearer '.$this->accessToken);
        return $this->client->get($this->url.'/v1/localization/zone/' . $this->localizationProvider, $data);
    }
}
