<?php

namespace Whiz\Laravel\API\Services;

use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Throwable;
use Whiz\Laravel\API\Entities\AuthEntity;
use Whiz\Laravel\API\Entities\AbilityEntity;
use Whiz\Laravel\API\Entities\PersonEntity;
use Whiz\Laravel\API\Entities\RoleEntity;
use Whiz\Laravel\API\Entities\UserEntity;

class UserService extends BaseService
{
    private $entities;

    /**
     * UserService constructor.
     * @throws Exception
     * @throws Throwable
     */
    public function __construct()
    {
        parent::__construct();
        $this->client->addHeader('Authorization', 'Bearer ' . $this->accessToken);
        $this->initEntities();
    }

    /**
     * @throws BindingResolutionException
     */
    private function initEntities()
    {
        $params =  ['client' => $this->client, 'url' => $this->url];
        $this->entities = [
            'auth' => app()->make(AuthEntity::class, $params),
            'ability' => app()->make(AbilityEntity::class, $params),
            'role' => app()->make(RoleEntity::class, $params),
            'user' => app()->make(UserEntity::class, $params),
            'person' => app()->make(PersonEntity::class, $params)
        ];
    }

    /**
     * @param $name
     * @return mixed
     * @throws Exception
     */
    public function __get($name)
    {
        try{
            return $this->entities[$name];
        }catch (Exception $exception){
            throw new Exception('Not found function '. $name. ' in UserService');
        }
    }
}
