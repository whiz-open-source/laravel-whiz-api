<?php

namespace Whiz\Laravel\API\Services;

use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Throwable;
use Whiz\Laravel\API\Entities\ListMail;
use Whiz\Laravel\API\Entities\SingleMail;

class MailingService extends BaseService
{
    private $entities;


    /**
     * UserService constructor.
     * @throws Exception
     * @throws Throwable
     */
    public function __construct()
    {
        parent::__construct();
        $this->client->addHeader('Authorization', 'Bearer ' . $this->accessToken);
        $this->initEntities();
    }

    /**
     * @throws BindingResolutionException
     */
    private function initEntities()
    {
        $params =  ['client' => $this->client, 'url' => $this->url];
        $this->entities = [
            'single' => app()->make(SingleMail::class, $params),
            'list' => app()->make(ListMail::class, $params)
        ];
    }

    /**
     * @param $name
     * @return mixed
     * @throws Exception
     */
    public function __get($name)
    {
        try{
            return $this->entities[$name];
        }catch (Exception $exception){
            throw new Exception('Not found function '. $name. ' in MailingService');
        }
    }

}
