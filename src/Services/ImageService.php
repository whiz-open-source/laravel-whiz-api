<?php

namespace Whiz\Laravel\API\Services;

use Exception;
use Throwable;

class ImageService extends BaseService
{

    /**
     * ImageService constructor.
     * @throws Exception
     * @throws Throwable
     */
    public function __construct()
    {
        parent::__construct();
        $this->client->addHeader('Authorization', 'Bearer ' . $this->accessToken);
    }


    /**
     * @param array $data
     * @param array $images
     * @param array $sizes
     * @return mixed
     */
    public function store($data = [], $images = [], $sizes = [])
    {
        $this->client->removeHeader('Content-Type');
        $newData = [];
        foreach ($data as $key => $value) {
            $newData[] = ['name' => $key, 'contents' => $value];
        }
        foreach ($sizes as $i => $size) {
            foreach ($size as $key => $value) {
                $newData[] = ['name' => 'sizes[' . $i . '][' . $key . ']', 'contents' => $value];
            }
        }
        foreach ($images as $image) {
            $newData[] = $image;
        }
        return $this->client->post($this->url . '/v1/image', $newData, false, true);
    }

    public function loadByUrl($data = [])
    {
        return $this->client->post($this->url . '/v1/image/url', $data);
    }
}
