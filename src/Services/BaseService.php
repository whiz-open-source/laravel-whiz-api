<?php

namespace Whiz\Laravel\API\Services;

use Exception;
use Illuminate\Cache\CacheManager;
use Illuminate\Support\Facades\Cache;
use Throwable;
use Whiz\Laravel\API\Clients\HttpClient;

class BaseService
{
    protected $client;
    protected $url;

    private $clientId;
    private $clientToken;

    protected $accessToken;

    /**
     * BaseService constructor.
     * @throws Exception
     * @throws Throwable
     */
    public function __construct()
    {
        $this->client = app()->make(HttpClient::class);
        $this->url = config('whiz.microservices.base_url');
        $this->clientId = config('whiz.client.client_id');
        $this->clientToken = config('whiz.client.client_secret');

        $this->validateConfig();
        $this->accessToken = $this->getAccessToken();
    }

    /**
     * @throws Throwable
     */
    private function validateConfig()
    {
        $conditions = $this->url === '' || $this->clientId == '' || $this->clientToken == '';
        throw_if($conditions, new Exception('Parameters in config is not valid.'));
    }

    /**
     * @return CacheManager|mixed
     * @throws Exception
     */
    public function getAccessToken()
    {
        if (!Cache::has('access_token_whiz')) {
            $response = $this->client->post($this->url . '/oauth/token', [
                'grant_type' => 'client_credentials',
                'client_id' => $this->clientId,
                'client_secret' => $this->clientToken,
                'scope' => '*'
            ]);
            $expire = $response->response->expires_in;
            if (!isset($expire)) {
                throw new Exception('Couldn\'t connect with Whiz API');
            }
            cache(['access_token_whiz' => $response->response->access_token], now()->addSeconds($expire));
        }
        return cache('access_token_whiz');
    }
}
