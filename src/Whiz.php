<?php

namespace Whiz\Laravel\API;

use Whiz\Laravel\API\Services\ExportService;
use Whiz\Laravel\API\Services\MailingService;
use Whiz\Laravel\API\Services\ImageService;
use Whiz\Laravel\API\Services\UserService;
use Whiz\Laravel\API\Services\LocalizationService;
use Exception;


class Whiz
{

    private $microServices;

    /**
     * ApiWhiz constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $this->microServices = $this->initMicroServices();
    }

  /**
   * @return array
   */
    private function initMicroServices()
    {
        return [
            'mailingService' => app()->make(MailingService::class),
            'imageService' => app()->make(ImageService::class),
            'userService' => app()->make(UserService::class),
            'localizationService' => app()->make(LocalizationService::class),
            'exportService' => app()->make(ExportService::class)
        ];
    }

    /**
     * @param $name
     * @return mixed
     * @throws Exception
     */
    public function __get($name)
    {
        try{
            return $this->microServices[$name];
        }catch (Exception $exception){
            throw new Exception('Not found microservice '. $name);
        }
    }
}
