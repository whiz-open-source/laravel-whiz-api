<?php

namespace Whiz\Laravel\API\Tests;

use Exception;
use Tests\TestCase;
use Whiz\Laravel\API\Tests\Traits\UtilForTest;
use Whiz\Laravel\API\Whiz;

class UserTest extends TestCase
{
    use UtilForTest;

    /**
     * @throws Exception
     */
    public function testIndex()
    {
        $apiWhiz = new Whiz();
        $response = $this->microservice($apiWhiz->userService->user, 'index');
        $this->assertArrayHasKey('status', (array)$response->response);
        $this->assertTrue($response->response->status);
    }


    public function testStore()
    {
        $apiWhiz = new Whiz();
        $person = $apiWhiz->userService->person->store([
            "name" => "Renzo",
            "last_name" => "Saravia",
            "birthdate" => "1989-02-01",
            "document_type" => "DNI",
            "document_number" => "45565709",
            "phone" => "123456789"
        ]);
        $ability = $apiWhiz->userService->ability->store("testing 9");
        $role = $apiWhiz->userService->role->store('logistic', [
            $ability->response->data->uuid
        ]);
        $response = $this->microservice($apiWhiz->userService->user, 'store', [
            [
                "email" => "logistic@admin.com",
                "password" => "123456",
                "person_uuid" => $person->response->data->uuid,
                "role" => $role->response->data->uuid
            ]
        ]);
        $this->assertArrayHasKey('status', (array)$response->response);
        $this->assertTrue($response->response->status);
    }

    public function testUpdate()
    {
        $apiWhiz = new Whiz();
        $person = $apiWhiz->userService->person->store([
            "name" => "Luis",
            "last_name" => "Benavides",
            "birthdate" => "1992-01-01",
            "document_type" => "DNI",
            "document_number" => "70186309",
            "phone" => "994312232"
        ]);
        $ability = $apiWhiz->userService->ability->store("testing 10");
        $role = $apiWhiz->userService->role->store('commercial', [
            $ability->response->data->uuid
        ]);
        $user = $apiWhiz->userService->user->store([
            "email" => "commercial@admin.com",
            "password" => "123456",
            "role" => $role->response->data->uuid,
            "person_uuid" => $person->response->data->uuid
        ]);
        $response = $this->microservice($apiWhiz->userService->user, 'update', [
            $user->response->data->uuid, [
                'password' => '654321'
            ]
        ]);
        $this->assertArrayHasKey('status', (array)$response->response);
        $this->assertTrue($response->response->status);
    }

    public function testShow()
    {
        $apiWhiz = new Whiz();
        $person = $apiWhiz->userService->person->store([
            "name" => "Michael",
            "last_name" => "Benavides",
            "birthdate" => "1997-04-17",
            "document_type" => "DNI",
            "document_number" => "70185082",
            "phone" => "123456789"
        ]);
        $ability = $apiWhiz->userService->ability->store("login");
        $role = $apiWhiz->userService->role->store('guest', [
            $ability->response->data->uuid
        ]);
        $user = $apiWhiz->userService->user->store([
            "email" => "guest@service.com",
            "password" => "123456",
            "role" => $role->response->data->uuid,
            "person_uuid" => $person->response->data->uuid
        ]);
        $response = $this->microservice($apiWhiz->userService->user, 'show', [$user->response->data->uuid]);
        $this->assertArrayHasKey('status', (array)$response->response);
        $this->assertTrue($response->response->status);
    }

    public function testDestroy()
    {
        $apiWhiz = new Whiz();
        $person = $apiWhiz->userService->person->store([
            "name" => "Juan",
            "last_name" => "Acevedo",
            "birthdate" => "1993-02-06",
            "document_type" => "DNI",
            "document_number" => "70186311",
            "phone" => "123456789"
        ]);
        $ability = $apiWhiz->userService->ability->store("review");
        $role = $apiWhiz->userService->role->store('legal', [
            $ability->response->data->uuid
        ]);
        $user = $apiWhiz->userService->user->store([
            "email" => "juan@service.com",
            "password" => "123456",
            "role" => $role->response->data->uuid,
            "person_uuid" => $person->response->data->uuid
        ]);
        $response = $this->microservice($apiWhiz->userService->user, 'destroy', [$user->response->data->uuid]);
        $this->assertArrayHasKey('status', (array)$response->response);
        $this->assertTrue($response->response->status);
    }

}
