<?php

namespace Whiz\Laravel\API\Tests;

use Exception;
use Tests\TestCase;
use Whiz\Laravel\API\Tests\Traits\UtilForTest;
use Whiz\Laravel\API\Whiz;

class AbilityTest extends TestCase
{
    use UtilForTest;

    /**
     * @throws Exception
     */
    public function testIndex()
    {
        $apiWhiz = new Whiz();
        $response = $this->microservice($apiWhiz->userService->ability, 'index');
        $this->assertArrayHasKey('status', (array) $response->response);
        $this->assertTrue($response->response->status);
    }

    /**
     *
     */
    public function testStore()
    {
        $apiWhiz = new Whiz();
        $response = $this->microservice($apiWhiz->userService->ability, 'store', ['testing']);
        $this->assertArrayHasKey('status', (array) $response->response);
        $this->assertTrue($response->response->status);
    }

    /**
     *
     */
    public function testShow()
    {
        $apiWhiz = new Whiz();
        $ability = $this->microservice($apiWhiz->userService->ability, 'store', ['testing 1']);
        $response = $this->microservice($apiWhiz->userService->ability, 'show', [$ability->response->data->uuid]);
        $this->assertArrayHasKey('status', (array) $response->response);
        $this->assertTrue($response->response->status);
    }

    /**
     *
     */
    public function testUpdate()
    {
        $apiWhiz = new Whiz();
        $ability = $apiWhiz->userService->ability->store("testing 2");
        $response = $this->microservice($apiWhiz->userService->ability, 'update', [$ability->response->data->uuid, "new testing"]);
        $this->assertArrayHasKey('status', (array) $response->response);
        $this->assertEquals("new testing", $response->response->data->title);
        $this->assertTrue($response->response->status);
    }

    /**
     *
     */
    public function testDestroy()
    {
        $apiWhiz = new Whiz();
        $ability = $apiWhiz->userService->ability->store("testing 3");
        $response = $this->microservice($apiWhiz->userService->ability, 'destroy', [$ability->response->data->uuid]);
        $this->assertArrayHasKey('status', (array) $response->response);
        $this->assertTrue($response->response->status);
    }
}
