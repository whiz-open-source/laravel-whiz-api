<?php

namespace Whiz\Laravel\API\Tests\Traits;

use Exception;
use Illuminate\Support\Facades\Log;

trait UtilForTest
{
    public function extractException(Exception $exception)
    {
        $jsonObj = json_decode($exception->getResponse()->getBody());
        return (object)['status' => 400, 'response' => (object)[
            'status' => isset($jsonObj->status)? $jsonObj->status:false,
            'message' => isset($jsonObj->message)? $jsonObj->message:null,
            'data' => isset($jsonObj->data)? $jsonObj->data:null
        ]];
    }

    public function microservice($callback, $function, $params = [])
    {
        try {
            $response = $callback->{$function}(...$params);
        } catch (Exception $exception) {
            Log::error($exception->getMessage(), [$exception]);
            $response = $this->extractException($exception);
        }
        return $response;
    }
}
