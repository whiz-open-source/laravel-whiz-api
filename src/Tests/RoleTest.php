<?php

namespace Whiz\Laravel\API\Tests;

use Exception;
use Tests\TestCase;
use Whiz\Laravel\API\Tests\Traits\UtilForTest;
use Whiz\Laravel\API\Whiz;

class RoleTest extends TestCase
{
    use UtilForTest;

    /**
     * @throws Exception
     */
    public function testIndex()
    {
        $apiWhiz = new Whiz();
        $response = $this->microservice($apiWhiz->userService->role, 'index');
        $this->assertArrayHasKey('status', (array)$response->response);
        $this->assertTrue($response->response->status);
    }


    /**
     *
     */
    public function testStore()
    {
        $apiWhiz = new Whiz();
        $ability = $apiWhiz->userService->ability->store("testing 4");
        $response = $this->microservice($apiWhiz->userService->role, 'store', ['admin', [
            $ability->response->data->uuid
        ]]);
        $this->assertArrayHasKey('status', (array)$response->response);
        $this->assertTrue($response->response->status);
    }


    /**
     *
     */
    public function testUpdate()
    {
        $apiWhiz = new Whiz();
        $ability1 = $apiWhiz->userService->ability->store("testing 5");
        $role = $apiWhiz->userService->role->store("user", [
            $ability1->response->data->uuid
        ]);
        $ability2 = $apiWhiz->userService->ability->store("testing 6");
        $response = $this->microservice($apiWhiz->userService->role, 'update', [
            $role->response->data->uuid, [
                "title" => "user new",
                "abilities" => [$ability1->response->data->uuid, $ability2->response->data->uuid]
            ]
        ]);
        $this->assertArrayHasKey('status', (array)$response->response);
        $this->assertTrue($response->response->status);
    }

    /**
     *
     */
    public function testShow()
    {
        $apiWhiz = new Whiz();
        $ability1 = $apiWhiz->userService->ability->store("testing 8");
        $role = $apiWhiz->userService->role->store("user_guest", [
            $ability1->response->data->uuid
        ]);
        $response = $this->microservice($apiWhiz->userService->role, 'show', [
            $role->response->data->uuid
        ]);
        $this->assertArrayHasKey('status', (array)$response->response);
        $this->assertTrue($response->response->status);
    }

    /**
     *
     */
    public function testDestroy()
    {
        $apiWhiz = new Whiz();
        $ability = $apiWhiz->userService->ability->store("testing 7");
        $role = $apiWhiz->userService->role->store("superadmin", [
            $ability->response->data->uuid
        ]);
        $response = $this->microservice($apiWhiz->userService->role, 'destroy', [
            $role->response->data->uuid
        ]);
        $this->assertArrayHasKey('status', (array)$response->response);
        $this->assertTrue($response->response->status);
    }
}
