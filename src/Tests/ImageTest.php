<?php

namespace Whiz\Laravel\API\Tests;

use Exception;
use Tests\TestCase;
use Whiz\Laravel\API\Tests\Traits\UtilForTest;
use Whiz\Laravel\API\Whiz;

class ImageTest extends TestCase
{
    use UtilForTest;

    /**
     * @throws Exception
     */
    public function testStore()
    {
        $apiWhiz = new Whiz();
        $stream = fopen(storage_path('app/public/avatar.png'), "r");
        $response = $this->microservice($apiWhiz->imageService, 'store', [
            [
                'table_name' => 'laravel_whiz_api', 'table_key' => 1
            ], [
                ['name' => 'image[]', 'contents' => $stream]
            ]
        ]);
        $this->assertArrayHasKey('status', (array)$response->response);
        $this->assertTrue($response->response->status);
    }
}
