<?php

namespace Whiz\Laravel\API\Entities;

class BaseEntity
{
    protected $url;
    protected $client;

    public function __construct($url,  $client)
    {
        $this->url = $url;
        $this->client = $client;
    }
}
