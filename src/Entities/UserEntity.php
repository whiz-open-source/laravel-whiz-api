<?php

namespace Whiz\Laravel\API\Entities;

class UserEntity extends BaseEntity
{

  /**
   * @param array $filters
   * @return mixed
   */
  public function index($filters = [])
  {
    return $this->client->get($this->url . '/v1/user', $filters);
  }

  /**
   * @param $data
   * @return mixed
   */
  public function store($data)
  {
    return $this->client->post($this->url . '/v1/user', $data);
  }


  /**
   * @param $uuid
   * @param array $data
   * @return mixed
   */
  public function show($uuid, $data = [])
  {
    return $this->client->get($this->url . '/v1/user/' . $uuid, $data);
  }

  /**
   * @param $uuid
   * @param array $data
   * @return mixed
   */
  public function update($uuid, $data = [])
  {
    return $this->client->patch($this->url . '/v1/user/' . $uuid, $data);
  }

  /**
   * @param $uuid
   * @return mixed
   */
  public function destroy($uuid)
  {
    return $this->client->delete($this->url . '/v1/user/' . $uuid);
  }


  /*** Custom Methods ***/

  /**
   * @param $email
   * @param $role
   * @return mixed
   */
  public function showUserByEmail($email, $role)
  {
    $data = ['email' => $email];
    if (is_array($role)) {
      $data['roles[]'] = $role;
    } else {
      $data['role'] = $role;
    }
    return $this->client->get($this->url . '/v1/user/email', $data);
  }

  /**
   * @param $dataUser
   * @param $dataPerson
   * @param bool $overWrite
   * @return mixed
   */
  public function storeWithPerson($dataUser, $dataPerson, $overWrite = false)
  {
    $data['user'] = $dataUser;
    $data['person'] = $dataPerson;
    $data['over_write'] = $overWrite;
    return $this->client->post($this->url . '/v1/user/person', $data);
  }

  /**
   * @param $user
   * @param $person
   * @param array $data
   * @return mixed
   */
  public function updateWithPerson($user, $person, $data = [])
  {
    return $this->client->patch($this->url . '/v1/user/' . $user . '/person/' . $person, $data);
  }

  /**
   * @param $email
   * @param $role
   * @return mixed
   */
  public function passwordTokenRecovery($email, $role)
  {
    $data['email'] = $email;
    $data['role'] = $role;
    return $this->client->post($this->url . '/v1/password/email', $data);
  }

  /**
   * @param $token
   * @param array $data
   * @return mixed
   */
  public function changePasswordWithToken($token, $data = [])
  {
    return $this->client->post($this->url . '/v1/password/' . $token, $data);
  }
}
