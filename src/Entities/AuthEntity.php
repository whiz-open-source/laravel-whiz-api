<?php

namespace Whiz\Laravel\API\Entities;

class AuthEntity extends BaseEntity
{
    /**
     * @param $email
     * @param $password
     * @param $role
     * @return mixed
     */
    public function login($email, $password, $role)
    {
        $data = [
            'email' => $email,
            'password' => $password,
            'role' => $role
        ];
        return $this->client->post($this->url . '/v1/login', $data);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function forgotPassword($data)
    {
        return $this->client->post($this->url . '/v1/password/email', $data);
    }


    /**
     * @param $token
     * @return mixed
     */
    public function findPassword($token)
    {
        return $this->client->get($this->url . '/v1/password/' . $token);
    }


    /**
     * @param $token
     * @param array $data
     * @return mixed
     */
    public function resetPassword($token, $data = [])
    {
        return $this->client->post($this->url . '/v1/password/' . $token, $data);
    }

    /**
     * @param $field
     * @param $username
     * @param $role
     * @return mixed
     */
    public function loginSend($field, $username, $role)
    {
        $data = [
            'field' => $field,
            'username' => $username,
            'role' => $role
        ];
        return $this->client->post($this->url . '/v1/login/send', $data);
    }

    /**
     * @param $field
     * @param $username
     * @param $code
     * @param $role
     * @return mixed
     */
    public function loginVerify($field, $username, $code, $role)
    {
        $data = [
            'field' => $field,
            'username' => $username,
            'code' => $code,
            'role' => $role
        ];
        return $this->client->post($this->url . '/v1/login/verify', $data);
    }
}
