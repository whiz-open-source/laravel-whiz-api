<?php

namespace Whiz\Laravel\API\Entities;

class PersonEntity extends BaseEntity
{
    /**
     * @param array $filters
     * @return mixed
     */
    public function index($filters = [])
    {
        return $this->client->get($this->url . '/v1/person', $filters);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function store($data)
    {
        return $this->client->post($this->url . '/v1/person', $data);
    }

    /**
     * @param $uuid
     * @return mixed
     */
    public function show($uuid)
    {
        return $this->client->get($this->url . '/v1/person/' . $uuid);
    }


    /**
     * @param $data
     * @param $uuid
     * @return mixed
     */
    public function update($data, $uuid)
    {
        return $this->client->patch($this->url . '/v1/person/' . $uuid, $data);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        return $this->client->delete($this->url . '/v1/person/' . $id);
    }
}
