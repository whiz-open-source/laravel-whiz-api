<?php


namespace Whiz\Laravel\API\Entities;


class RoleEntity extends BaseEntity
{
    /**
     * @return mixed
     */
    public function index()
    {
        return $this->client->get($this->url . '/v1/role');
    }

    /**
     * @param $name
     * @param array $abilities
     * @return mixed
     */
    public function store($name, $abilities = [])
    {
        return $this->client->post($this->url . '/v1/role', [
            'title' => $name,
            'abilities' => $abilities
        ]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->client->get($this->url . '/v1/role/' . $id);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, $data = [])
    {
        return $this->client->patch($this->url . '/v1/role/' . $id, $data);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        return $this->client->delete($this->url . '/v1/role/' . $id);
    }
}
