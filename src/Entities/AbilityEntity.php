<?php

namespace Whiz\Laravel\API\Entities;

class AbilityEntity extends BaseEntity
{
    /**
     * @return mixed
     */
    public function index()
    {
        return $this->client->get($this->url . '/v1/ability');
    }

    /**
     * @param $title
     * @return mixed
     */
    public function store($title)
    {
        return $this->client->post($this->url . '/v1/ability', [
            'title' => $title
        ]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->client->get($this->url . '/v1/ability/' . $id);
    }

    /**
     * @param $id
     * @param $title
     * @return mixed
     */
    public function update($id, $title)
    {
        return $this->client->patch($this->url . '/v1/ability/' . $id, [
            'title' => $title
        ]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        return $this->client->delete($this->url . '/v1/ability/' . $id);
    }
}
