<?php

namespace Whiz\Laravel\API\Entities;

use Exception;
use Throwable;

class SingleMail extends BaseEntity
{
    private $sender;
    private $addressee;
    private $subject;
    private $template;
    private $cc;


    /**
     * @param $sender
     * @param $addressee
     * @param $subject
     * @param $template
     * @return $this
     * @throws Throwable
     */
    public function prepareMail($subject, $sender, $addressee, $template = null)
    {
        $this->sender = $sender;
        $this->addressee = $addressee;
        $this->subject = $subject;
        if (!is_null($template)) {
            $this->template = $template;
        }
        return $this;
    }

    /**
     * @param $view
     * @param array $data
     * @return SingleMail
     * @throws Throwable
     */
    public function setView($view, $data = [])
    {
        $this->template = view($view, $data)->render();
        return $this;
    }

    /**
     * @param $email
     * @param $name
     * @return $this
     */
    public function addCC($email, $name)
    {
        $this->cc[] = ['email' => $email, 'name' => $name];
        return $this;
    }

    /**
     * @return mixed
     * @throws Throwable
     * @throws Exception
     */
    public function sendMail()
    {
        if ($this->subject === '') throw new Exception('subject is required');
        if (empty($this->sender)) throw new Exception('sender is required');
        if(empty($this->template)) throw new Exception('template is not configured');

        $data = [
            'sender' => $this->sender,
            'addressee' => $this->addressee,
            'subject' => $this->subject,
            'template' => $this->template
        ];
        if (!is_null($this->cc) && is_array($this->cc)) {
            $data['cc'] = $this->cc;
        }
        return $this->client->post($this->url . '/v1/mail/send', $data);
    }
}
