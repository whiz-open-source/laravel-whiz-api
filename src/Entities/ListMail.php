<?php

namespace Whiz\Laravel\API\Entities;

use Exception;
use Throwable;

class ListMail extends BaseEntity
{
    private $template;

    /**
     * @return mixed
     */
    public function index()
    {
        return $this->client->get($this->url . '/v1/list');
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function store($data = [])
    {
        return $this->client->post($this->url . '/v1/list', $data);
    }

    /**
     * @param null $uuid
     * @param $data
     * @return mixed
     */
    public function update($uuid, $data)
    {
        if (is_null($uuid)) throw new Exception('uuid is required');
        return $this->client->patch($this->url . '/v1/list/' . $uuid, $data);
    }
    /**
     * @param null $uuid
     * @return mixed
     * @throws Exception
     */
    public function destroy($uuid)
    {
        if (is_null($uuid)) throw new Exception('uuid is required');
        return $this->client->delete($this->url . '/v1/list/' . $uuid);
    }

    /**
     * @param null $uuid
     * @return mixed
     * @throws Exception
     */
    public function show($uuid)
    {
        if (is_null($uuid)) throw new Exception('uuid is required');
        return $this->client->get($this->url . '/v1/list/' . $uuid);
    }

    /**
     * @param null $uuid
     * @param array $people
     * @return mixed
     * @throws Exception
     */
    public function addPeople($uuid, $people = [])
    {
        if (is_null($uuid)) throw new Exception('uuid is required');
        if (empty($people)) throw new Exception('people is required');
        return $this->client->post($this->url . '/v1/list/' . $uuid . '/mail/add', $people);
    }

    /**
     * @param null $uuid
     * @param array $people
     * @return mixed
     * @throws Exception
     */
    public function removePeople($uuid, $people = [])
    {
        if (is_null($uuid)) throw new Exception('uuid is required');
        if (empty($people)) throw new Exception('people is required');
        return $this->client->post($this->url . '/v1/list/' . $uuid . '/mail/remove', $people);
    }

    /**
     * @param $view
     * @param array $data
     * @return ListMail
     * @throws Throwable
     */
    public function setView($view, $data = [])
    {
        $this->template = view($view, $data)->render();
        return $this;
    }


    /**
     * @param null $uuid
     * @param string $subject
     * @param array $sender
     * @return
     * @throws Exception
     */
    public function send($uuid, $subject = '', $sender = [])
    {
        if (is_null($uuid)) throw new Exception('uuid is required');
        if ($subject === '') throw new Exception('subject is required');
        if (empty($sender)) throw new Exception('sender is required');
        if (empty($this->template)) throw new Exception('template is not configured');

        $data = [
            'sender' => [
                'email' => $sender['email'], 'name' => $sender['name']
            ],
            'subject' => $subject,
            'template' => $this->template
        ];
        return $this->client->post($this->url . '/v1/list/' . $uuid . '/mail/send', $data);
    }

    /**
     * @param null $uuid
     * @param int $page
     * @return mixed
     * @throws Exception
     */
    public function showPeople($uuid, $page = 1)
    {
        if (is_null($uuid)) throw new Exception('uuid is required');
        return $this->client->get($this->url . '/v1/list/' . $uuid . '/people', [ 'page'=> $page ]);
    }
}
