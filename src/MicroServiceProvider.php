<?php

namespace Whiz\Laravel\API;

use Illuminate\Support\ServiceProvider;


class MicroServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/whiz.php' => config_path('whiz.php'),
        ]);
    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/whiz.php','whiz'
        );
    }
}
