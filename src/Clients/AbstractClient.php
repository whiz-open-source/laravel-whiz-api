<?php

namespace Whiz\Laravel\API\Clients;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;


/**
 * Class AbstractClient
 * @package App\Eloquent\Clients
 */
abstract class AbstractClient
{

    /**
     * @var array
     */
    protected $defaultHeaders = [
        'Content-Type' => 'application/json',
    ];

    /**
     * @var array
     */
    private $headers;


    /**
     * @var Client
     */
    private $client;

    /**
     * AbstractClient constructor.
     * @param array $headers
     */
    public function __construct($headers = [])
    {
        $this->client = new Client();
        $this->headers = $headers;
        $this->setDefaultHeaders();
    }

    /**
     * @return $this
     */
    public function setDefaultHeaders()
    {
        foreach ($this->defaultHeaders as $name => $value) {
            $this->headers[$name] = $value;
        }
        return $this;
    }


    /**
     * @param $name
     * @param $value
     * @return $this
     */
    public function addHeader($name, $value)
    {
        $this->headers[$name] = $value;
        return $this;
    }

    /**
     * @param $name
     * @return $this
     */
    public function removeHeader($name)
    {
        if (isset($this->headers[$name])) {
            unset($this->headers[$name]);
        }
        return $this;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param array $headers
     */
    public function setHeaders($headers)
    {
        $this->headers = $headers;
    }


    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param $url
     * @param array $query
     * @return object
     */
    public function get($url, $query = [])
    {
        $response = $this->client->get($url, ['query' => $query, 'headers' => $this->headers]);
        return $this->formatData($response);
    }

    /**
     * @param $url
     * @param $data
     * @param bool $json
     * @param bool $multipart
     * @return object
     */
    public function post($url, $data, $json = true, $multipart = false)
    {
        $data = $this->clearNulls($data);
        $content['headers'] = $this->headers;
        if ($multipart){
            $content['multipart'] =  $data;
        }else{
            if ($json) {
                $content['json'] = $data;
            } else {
                $content['form_params'] = $data;
            }
        }
        $response = $this->client->post($url, $content);
        return $this->formatData($response);
    }

    /**
     * @param $url
     * @param $data
     * @param bool $json
     * @return object
     */
    public function put($url, $data, $json=true)
    {
        $data = $this->clearNulls($data);
        if($json) {
            $response = $this->client->put($url, ['json' => $data, 'headers' => $this->headers]);
        }else{
            $response = $this->client->put($url, ['form_params' => $data, 'headers' => $this->headers]);
        }
        return $this->formatData($response);
    }

    /**
     * @param $url
     * @param $data
     * @param bool $json
     * @return object
     */
    public function patch($url, $data, $json=true)
    {
        $data = $this->clearNulls($data);
        if($json) {
            $response = $this->client->patch($url, ['json' => $data, 'headers' => $this->headers]);
        }else{
            $response = $this->client->patch($url, ['form_params' => $data, 'headers' => $this->headers]);
        }
        return $this->formatData($response);
    }

    /**
     * @param $url
     * @return object
     */
    public function delete($url)
    {
        $response = $this->client->delete($url, ['headers' => $this->headers]);
        return $this->formatData($response);
    }


    /**
     * @param ResponseInterface $response
     * @return object
     */
    public function formatData(ResponseInterface $response)
    {
        return (object) ['response' => json_decode((string) $response->getBody()), 'status' => $response->getStatusCode()];
    }

    /**
     * @param $data
     * @return array
     */
    private function clearNulls($data) {
        $return = [];
        foreach ($data as $index => $value) {
            if(!is_null($value)) {
                $return[$index] = $value;
            }
        }
        return $return;
    }
}
