# Laravel Whiz API
![Packagist](https://img.shields.io/packagist/dm/whiz/laravel-whiz-api?style=plastic)

This package, manage the connections with microservices.

## Installation 

```sh
composer require whiz/laravel-whiz-api
```

## Configure and usage

1. Add to .env file this parameters

```sh
WHIZ_API_BASE_URL=
WHIZ_CLIENT_ID=
WHIZ_CLIENT_SECRET=
```

2. Add to controller as to injection of dependencies.

```sh
    public function functionExample(Whiz $whiz){
        $response = $whiz->userService->person->store([
            "name" => "John",
            "last_name" => "Doe",
            "birthdate" => "1992-01-01",
            "document_type" => "DNI",
            "document_number" => "12345678",
            "phone" => "123456789"
        ]);
        return response()->json($response->response->data);
    }
```

## Available Services

 - userService (user and authentication)
 - imageService (cdn image and optimization formats)
 - mailingService (transactional and massive mailing)
 - localizationService (localization, boundaries and more)
 
## UserService

     1. user
        - index
        - show 
        - store 
        - update
        - destroy
        - showUserByEmail
        - storeWithPerson
        - updateWithPerson
 
     2. person
        - index
        - show 
        - store 
        - update
        - destroy

     3. role
        - index
        - show 
        - store 
        - update
        - destroy
        
     4. ability
        - index
        - show 
        - store 
        - update
        - destroy
        
     5. auth
        - login
        - forgotPassword
        - findPassword
        - resetPassword
    
## ImageService 
      - store
      - loadByUrl
  
## MailingService
  
    single (in order and use with chains)    
      - prepareMail
      - setView
      - addCC
      - sendMail
  
    list
      - index
      - store 
      - update
      - destroy
      - show
      - addPeople
      - removePeople
      - showPeople
      - setView
      - send 
  
## LocalizationService (not tested)
      - zoneByProvider
      - dangerZone   
