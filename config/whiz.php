<?php

return [
    'client' => [
        'client_id' => env('WHIZ_CLIENT_ID'),
        'client_secret' => env('WHIZ_CLIENT_SECRET'),
    ],

    'microservices' => [
        'base_url' => env('WHIZ_API_BASE_URL')
    ],

    'providers' => [
        'localization' => 'chazki'
    ]
];
